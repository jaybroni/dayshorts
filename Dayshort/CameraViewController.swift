//
//  CameraViewController.swift
//  Dayshort
//
//  Created by Jay on 3/9/15.
//  Copyright (c) 2015 Appsilog LLC. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary
import CoreLocation
import AddressBook

class CameraViewController: UIViewController, PBJVisionDelegate, UIGestureRecognizerDelegate,CLLocationManagerDelegate {

    var assetLibrary = ALAssetsLibrary()
    var captureSession: AVCaptureSession?
    var videoCaptureOutput: AVCaptureVideoDataOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var currentVideo = NSDictionary()
    var recording: Bool = false
    var locationFixAchieved : Bool = false
    var locManager: CLLocationManager!
    var seenError: Bool = false
    var timeFormat: NSDateFormatter = NSDateFormatter()
    
    @IBOutlet weak var flashIcon: UIImageView!
    @IBOutlet weak var flipCameraIcon: UIImageView!
    @IBOutlet weak var pressAndHoldLabel: UILabel!
    @IBOutlet weak var previewView: UIView!
    @IBAction func showTodaysVideos(sender: AnyObject) {
        
       NSNotificationCenter.defaultCenter().postNotificationName("showLibraryVC", object: true, userInfo: nil)
    }
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override  func viewDidLoad(){
        
        previewLayer = PBJVision.sharedInstance().previewLayer
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer?.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        previewView?.layer.addSublayer(previewLayer)
       
        
        setup()
        initLocationManager()
        
        
        timeFormat.dateFormat = "hh:mm a"
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateTime:", userInfo: nil, repeats: true)
    }
    
    func updateTime(sender: AnyObject){
        var time: String = timeFormat.stringFromDate(NSDate())
        self.time.text = time
        
    }
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestAlwaysAuthorization()
        locManager.startUpdatingLocation()

    }
    
    override func viewDidAppear(animated: Bool) {
        
        
    }

    

    func setup(){
        let vision = PBJVision.sharedInstance()
        vision.delegate = self
        vision.cameraMode = PBJCameraMode.Video
        vision.cameraOrientation = PBJCameraOrientation.LandscapeRight
        vision.focusMode = PBJFocusMode.ContinuousAutoFocus
        vision.outputFormat = PBJOutputFormat.Widescreen
        vision.audioCaptureEnabled = true
        PBJVision.sharedInstance().startPreview()
        
        var longPressGesture = UILongPressGestureRecognizer(target: self, action: "startRecording:")
        self.view.addGestureRecognizer(longPressGesture)
     
        var flipCameraGesture = UITapGestureRecognizer(target: self, action: "flipCamera:")
        flipCameraGesture.delegate = self
        self.flipCameraIcon.addGestureRecognizer(flipCameraGesture)
        
        var flashGesture = UITapGestureRecognizer(target: self, action: "flash:")
        flashGesture.delegate = self
        self.flashIcon.addGestureRecognizer(flashGesture)
        
        
    }
    
   
    func flash(r: UIGestureRecognizer)
    {
        let vision = PBJVision.sharedInstance()
        println(vision.flashAvailable)
        if(vision.flashAvailable == true)
        {
           vision.flashMode = vision.flashMode == PBJFlashMode.Off ? PBJFlashMode.On : PBJFlashMode.Off
        }
       

        //PBJVision.sharedInstance().flashMode = PBJFlashMode.
    }
    
    func flipCamera(r: UIGestureRecognizer)
    {
        let vision = PBJVision.sharedInstance()
        vision.cameraDevice = vision.cameraDevice == PBJCameraDevice.Back ? PBJCameraDevice.Front : PBJCameraDevice.Back
        
        println("TODO: change camera icon image when flipping views")
        
    }
    
    func startRecording(r: UIGestureRecognizer)
    {
        switch(r.state){
        case UIGestureRecognizerState.Began:
            
            if(!recording){
                self.startCapture()
            }else
            {
                self.resumeCapture()
            }
        case UIGestureRecognizerState.Ended, UIGestureRecognizerState.Cancelled, UIGestureRecognizerState.Failed:
            self.endCapture()
        default:
            break
        }
    }
    func startCapture()
    {
        pressAndHoldLabel.hidden = true
        PBJVision.sharedInstance().startVideoCapture()
    }
    
    func resumeCapture()
    {
        PBJVision.sharedInstance().resumeVideoCapture()
    }
    
    func pauseCapture()
    {
        PBJVision.sharedInstance().pauseVideoCapture()
    }
    
    func endCapture()
    {
        pressAndHoldLabel.hidden = false
        PBJVision.sharedInstance().endVideoCapture()
    }
    func visionDidStartVideoCapture(vision: PBJVision!) {
        recording = true
    }
    
    func vision(vision: PBJVision!, capturedVideo videoDict: NSDictionary!, error: NSError!) {
        self.currentVideo = videoDict
        
        let videoPath = self.currentVideo.objectForKey(PBJVisionVideoPathKey) as NSString

        assetLibrary.writeVideoAtPathToSavedPhotosAlbum(NSURL(string: videoPath)!, completionBlock: {(url: NSURL!, error: NSError!)  in
            println("URL %@",url);
            if error != nil{
            }
        })
        
       
    }

    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        locManager.stopUpdatingLocation()
        if (error != nil) {
             print(error)
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if (locationFixAchieved == false) {
            locationFixAchieved = true
            var locationArray = locations as NSArray
            var locationObj = locationArray.lastObject as CLLocation
            var coord = locationObj.coordinate
            
            let geoCoder = CLGeocoder()
            let newLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            
            geoCoder.reverseGeocodeLocation(newLocation, completionHandler:
                {(placemarks: [AnyObject]!, error: NSError!) in
                    
                    if error != nil {
                        println("Geocode failed with error: \(error.localizedDescription)")
                    }
                    
                    if placemarks.count > 0 {
                        let placemark = placemarks[0] as CLPlacemark
                        
                        self.location.text = placemark.name
                        println("\(placemark.name)")
                    }
            })
            
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
    }
}
