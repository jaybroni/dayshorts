//
//  ContainerViewController.swift
//  Dayshort
//
//  Created by Jay on 3/10/15.
//  Copyright (c) 2015 Appsilog LLC. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController
{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var cameraVC: CameraViewController = mainStoryboard.instantiateViewControllerWithIdentifier("cameraVC") as CameraViewController
        var libraryVC: LibraryViewController = mainStoryboard.instantiateViewControllerWithIdentifier("libraryVC") as LibraryViewController
        
        self.addChildViewController(libraryVC)
        self.scrollView!.addSubview(libraryVC.view)
        
        self.addChildViewController(cameraVC)
        self.scrollView!.addSubview(cameraVC.view)
        
        var adminFrame: CGRect = libraryVC.view.frame
        adminFrame.origin.x = adminFrame.width;
        cameraVC.view.frame = adminFrame
        
        var scrollWidth: CGFloat = 2 * self.view.frame.width
        var scrollHeight: CGFloat = self.view.frame.size.height
        self.scrollView!.contentSize = CGSizeMake(scrollWidth, scrollHeight)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "notifyShowCameraVC:", name:"showCameraVC", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "notifyShowLibraryVC:", name:"showLibraryVC", object: nil)

        
        self.showCameraVC(false)

    }
    
    func notifyShowCameraVC(notification: NSNotification)
    {
        let animated = notification.object as Bool
        showCameraVC(animated)
    }
    
    func notifyShowLibraryVC(notification: NSNotification)
    {
        println("notifyShowLibraryVC")
        let showToday = notification.object as Bool
        showLibraryVC(showToday)
    }
    
    func showLibraryVC(showToday: Bool)
    {
        let startingPoint = CGPointMake(0, 0)
        self.scrollView!.setContentOffset(startingPoint, animated: true)
    }
    
    func showCameraVC(animated: Bool)
    {
        let startingPoint = CGPointMake(self.view.frame.size.width, 0)
        self.scrollView!.setContentOffset(startingPoint, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}