//
//  ThumbnailCollectionViewCell.swift
//  Dayshort
//
//  Created by Jay on 3/10/15.
//  Copyright (c) 2015 Appsilog LLC. All rights reserved.
//

import UIKit

class ThumbnailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    
}
