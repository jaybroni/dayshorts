//
//  LibraryViewController.swift
//  Dayshort
//
//  Created by Jay on 3/10/15.
//  Copyright (c) 2015 Appsilog LLC. All rights reserved.
//

import UIKit
import AssetsLibrary
import CoreLocation

import Foundation






class AssetsLib : ALAssetsLibrary {
    class var sharedInstance : AssetsLib{
        struct Static{
            static let instance: AssetsLib = AssetsLib()
        }
        
        return Static.instance
    }
    
    override init(){
        
    }
}




class LibraryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    
    var selectedDate: NSDate = NSDate()  // Default to today's date
    
    var videoData: NSArray? = NSArray() {
        didSet{
          self.libraryCollection.reloadData()
        }
    }
    
    @IBOutlet weak var libraryCollection: UICollectionView!
    
    @IBOutlet weak var cameraIcon: UIImageView!
    
    override func viewDidLoad(){
        
        self.libraryCollection.delegate = self
        self.libraryCollection.dataSource = self
        
        let tapCameraGesture = UITapGestureRecognizer(target: self, action: "showCameraVC:")
        self.cameraIcon.addGestureRecognizer(tapCameraGesture)
        
        
        let library = AssetsLib.sharedInstance
        
        let collector: NSMutableArray = NSMutableArray()
        library.enumerateGroupsWithTypes(ALAssetsGroupType(ALAssetsGroupAll), usingBlock: {
            (group: ALAssetsGroup!, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            if(group != nil)
            {
                group?.setAssetsFilter(ALAssetsFilter.allVideos())
                group.enumerateAssetsUsingBlock({
                    (asset: ALAsset!, index: Int, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                    if asset != nil{
                        var rep: ALAssetRepresentation = asset.defaultRepresentation()
                        let url = rep.url()
                        
                        
                        var iref: Unmanaged<CGImage> = asset.aspectRatioThumbnail()

                       
                        let date = asset.valueForProperty(ALAssetPropertyDate) as NSDate
                        let selectedDate = self.selectedDate
                        
                        
                        let calendar = NSCalendar.currentCalendar()
                        let componentsToday = calendar.components(.YearCalendarUnit | .MonthCalendarUnit | .DayCalendarUnit, fromDate: selectedDate)
                       
                        
                        let dif = calendar.compareDate(selectedDate, toDate: date, toUnitGranularity: NSCalendarUnit.DayCalendarUnit)
                        
                        if (dif == .OrderedSame)
                        {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateStyle = .MediumStyle
                            let timeFormatter = NSDateFormatter()
                            timeFormatter.dateFormat = "h:mm a"
                            var d: NSString = dateFormatter.stringFromDate(date)
                            var t: NSString = timeFormatter.stringFromDate(date)
                            var dt: NSString = "\(d) \(t)"
                            collector.addObject(asset)
                            
                            
                        }
                    
                    }
                })
                println(collector.count)
                self.videoData = collector
            }
            
            }, failureBlock: {(error: NSError!) in
                println(error)
                
        })
    }
    
    func getLocationName(location: CLLocation)
    {
        let geoCoder = CLGeocoder()
        let newLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(newLocation, completionHandler:
            {(placemarks: [AnyObject]!, error: NSError!) in
                
                if error != nil {
                    println("Geocode failed with error: \(error.localizedDescription)")
                }
                
                if placemarks.count > 0 {
                    let placemark = placemarks[0] as CLPlacemark
                    
                    println("\(placemark.name)")
                }
        })
    }
    
    func showCameraVC(r: UIGestureRecognizer)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("showCameraVC", object: true, userInfo: nil)
    }
    
    
    // MARK: UICollectionViewDatasource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.videoData!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as ThumbnailCollectionViewCell
        let asset: ALAsset = self.videoData![indexPath.row] as ALAsset
        
        cell.backgroundColor = UIColor.whiteColor()
        if let thumbnail = asset.thumbnail()?.takeUnretainedValue() {
                cell.thumbnail.image = UIImage(CGImage: thumbnail)
            println(asset.defaultRepresentation().url())
        }
        
        // GPS location
        if let location: CLLocation = asset.valueForProperty(ALAssetPropertyLocation) as CLLocation!
        {
            self.getLocationName(location)
            
        }
        
        
        return cell
        
        
    }
    
    
    
}
